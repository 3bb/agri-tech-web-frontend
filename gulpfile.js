'use strict';

/*=======================================
 --Gulp Commons                        ||
 ======================================*/
var gulp = require('gulp');
var sass = require('gulp-sass'); //to be removed
var $ = require('gulp-load-plugins')({
    pattern: '*',
    replaceString: /\bgulp[\-.]/
});

var postcssProcessors = [ $.autoprefixer, $.cssnano ],
    paths = {
        scss: 'src/sass/',
        js: 'src/js/',
        fonts: 'src/resources/fonts/',
        img: 'src/resources/img/',
        templates: 'src/templates/',
        php: 'src/php/',
        dist: 'dist/'
    };


/*=======================================
 --Gulp tasks                           ||
 ======================================*/
// clean
gulp.task('clean', function (){
    return $.del( 'dist/*' );
});

//browser sync stuff
gulp.task('reload:css', function (){
    return $.browserSync.reload(paths.dist + 'css/**/*.css');
});

gulp.task('reload:templates', function (){
    return $.browserSync.reload([ '*.php', paths.templatesLocal + '*.php', paths.templates + '*.*' ])
});

//styles
gulp.task('build:css', function (){
    var vendors = gulp.src([
        'node_modules/bootstrap/css/bootstrap.css'
    ])
        .pipe( $.concat( 'vendors.css') )
        .pipe( $.postcss( postcssProcessors ) )
        .pipe( $.rename({ suffix: '.min' }) )
        .pipe( gulp.dest( 'dist/css') );

    var main = gulp.src( paths.scss + '**/*.scss' )
        .pipe( $.sourcemaps.init() )
        .pipe( sass().on( 'error', sass.logError ) )
        .pipe( $.postcss( postcssProcessors ) )
        .pipe( $.sourcemaps.write() )
        .pipe( $.rename({ suffix: '.min' }) )
        .pipe( gulp.dest('dist/css') );

    return $.mergeStream(main, vendors);
});

gulp.task('build:js', function (){
    var vendors = gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js'

    ])
        .pipe( $.concat('vendors.js') )
        .pipe( gulp.dest(paths.js + 'vendors') )
        // .pipe( $.uglify() )
        .pipe( $.rename({ suffix: '.min' }) )
        .pipe( gulp.dest(paths.dist + 'js') );

    var main = gulp.src(paths.js + 'main.js')
        .pipe( $.concat('main.js') )
        .pipe( gulp.dest(paths.js) )
        // .pipe( $.uglify() )
        .pipe( $.rename({ suffix: '.min' }) )
        .pipe( gulp.dest(paths.dist + 'js') );

    return $.mergeStream(main, vendors);
});

gulp.task('build:images', function (){
    return gulp.src(paths.img + '**/*.{png,jpg,jpeg,svg}')
    // .pipe($.tinypngCompress({
    //   key: 'RSzN-Is5Ge6A9U9gUGQQdJOgjJxgFaUa',
    //   sameDest: true,
    //   log: true,
    //   parallel: true,
    //   summarize: true,
    //   sigFile: '.tinypng-sigs'
    // }))
        .pipe(gulp.dest(paths.dist + 'img/'))
});

gulp.task('build:php', function (){
    return gulp.src(paths.php + '**/*.*')
        .pipe(gulp.dest(paths.dist + 'php/'))
});

gulp.task('build:fonts', function (){
    return gulp.src(paths.fonts + '**/*.*')
        .pipe(gulp.dest(paths.dist + 'fonts/'))
});

/*=======================================
 --Gulp watchers                       ||
 ======================================*/
gulp.task( 'watch:templates', function (){
    //watch php filefs form root and templates dir
    gulp.watch( [ '*.php', paths.templates + '*.*', paths.php + '*.*' ], [ 'reload:templates' ] );
});

gulp.task( 'watch:php', function (){
    //watch php filefs form root and templates dir
    gulp.watch( [ paths.php + '*.*' ], [ 'build:php', 'reload:templates' ] );
});


gulp.task( 'watch:js', function (){
    //watch scss files
    gulp.watch( paths.js + '*.js', [ 'build:js', $.browserSync.reload ] );
});

gulp.task( 'watch:sass', function (){
    //watch scss files
    gulp.watch( paths.scss + '**/*.scss', [ 'build:css'] );
});

gulp.task( 'watch:css', function (){
    gulp.watch( paths.dist + 'css/**/*.css', [ 'reload:css' ] );
});

gulp.task( 'watch:init', function (){
    $.connectPhp.server({}, function (){
        $.browserSync({
            proxy: '127.0.0.1:8000'
        });
    });
});

/*=======================================
 --Gulp global runners                 ||
 ======================================*/
gulp.task('build', [ 'build:css', 'build:js', 'build:fonts', 'build:images', 'build:php' ]);

gulp.task('watch', [ 'watch:init', 'watch:templates', 'watch:php', 'watch:sass', 'watch:css', 'watch:js' ]);

gulp.task('default', ['clean', 'build']);